﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kirinnee.ConsoleHelp;
using Kirinnee.Helper;

namespace TestConsole
{
    internal static class Program
    {
        internal static void Main(string[] args)
        {
            var exe = new CommandExecutor("Test", "Test console", "0.0.1",
                new List<ICommandObject>() {new Command1(), new Command2()});
            Console.WriteLine(exe._commands.Count());
            Async(args, exe).GetAwaiter().GetResult();
        }

        static async Task Async(string[] args, ICommandExecutor exe)
        {
            await exe.ExecuteCommand(args);
        }
    }


    internal class Command1 : ICommandObject
    {
        public string Name { get; } = "Upper";
        public string Description { get; } = "Print in uppercase";
        public IEnumerable<string> PossibleFlags { get; } = new[] {"upper"};
        public IEnumerable<string> PossibleVariables { get; } = new[] {"min", "max"};
        public string[] Command { get; } = {"cm1", "pu"};
        public string PerfectCommand => Command.JoinBy(" ");
        public string Usage { get; } = "cm1 pu <string> --upper --min=5 --max=10";

        public Task<CommandResult> Execute(string[] fullCommand, string[] rawArguments,
            Dictionary<string, string> variables, string[] flags)
        {
            var min = "0";
            var max = "100";
            if (variables.ContainsKey("min")) min = variables["min"];
            if (variables.ContainsKey("max")) max = variables["max"];
            if (max.ToInt() < min.ToInt())
            {
                return Task.FromResult(new CommandResult(false,
                    $"Min cannot be larger than Max! Min: {min}, Max: {max}"));
            }

            var p = rawArguments.JoinBy(" ");
            if (flags.Contains("upper")) p = p.ToUpper();


            if (p.Length < min.ToInt()) p = "0".Repeat(min.ToInt() - p.Length) + p;
            p = p.Take(max.ToInt());
            Console.WriteLine(p);
            return Task.FromResult(new CommandResult(true, "Success!"));
        }
    }


    internal class Command2 : ICommandObject
    {
        public string Name { get; } = "Upper";
        public string Description { get; } = "Print in lowercase";
        public IEnumerable<string> PossibleFlags { get; } = new[] {"lower"};
        public IEnumerable<string> PossibleVariables { get; } = new[] {"min", "max"};
        public string[] Command { get; } = {"cm1", "pl"};
        public string PerfectCommand => Command.JoinBy(" ");
        public string Usage { get; } = "cm1 pu <string> --lower --min=5 --max=10";

        public Task<CommandResult> Execute(string[] fullCommand, string[] rawArguments,
            Dictionary<string, string> variables, string[] flags)
        {
            var min = "0";
            var max = "100";
            if (variables.ContainsKey("min")) min = variables["min"];
            if (variables.ContainsKey("max")) max = variables["max"];
            if (max.ToInt() < min.ToInt())
            {
                return Task.FromResult(new CommandResult(false,
                    $"Min cannot be larger than Max! Min: {min}, Max: {max}"));
            }

            var p = rawArguments.JoinBy(" ");
            if (flags.Contains("lower")) p = p.ToLower();
            if (p.Length < min.ToInt()) p = "0".Repeat(min.ToInt() - p.Length) + p;
            p = p.Take(max.ToInt());
            Console.WriteLine(p);
            return Task.FromResult(new CommandResult(true, "Success!"));
        }
    }
}