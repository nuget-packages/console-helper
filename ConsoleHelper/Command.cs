﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kirinnee.ConsoleHelp
{
    public interface ICommandObject
    {
        string Name { get; }
        
        string Description { get; }
        
        IEnumerable<string> PossibleFlags { get; }

        IEnumerable<string> PossibleVariables { get; }

        string[] Command { get; }

        string PerfectCommand { get; }

        string Usage { get; }

        Task<CommandResult> Execute(string[] fullCommand, string[] rawArguments, Dictionary<string, string> variables,
            string[] flags);
    }
}