using System;

namespace Kirinnee.ConsoleHelp
{
    public class CommandResult
    {
        private readonly bool _success;
        private readonly string _message;

        public CommandResult(bool success, string message)
        {
            _success = success;
            _message = message;
        }

        public void Print()
        {
            Console.ForegroundColor = _success ? ConsoleColor.Green : ConsoleColor.Red;
            var word = _success ? "succeeded" : "failed";
            Console.WriteLine($"Command {word}: {_message}");
            Console.ResetColor();
            if (!_success)
            {
                throw new InvalidProgramException();
            }
        }
    }
}