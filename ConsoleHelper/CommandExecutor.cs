using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Kirinnee.Helper;

namespace Kirinnee.ConsoleHelp
{
    public class CommandExecutor : ICommandExecutor
    {
        public IEnumerable<ICommandObject> _commands;
        public string Description { get; }
        public string Version { get; }
        public string Name { get; }

        public CommandExecutor(string name, string des, string version, IEnumerable<ICommandObject> commands)
        {
            Name = name;
            Description = des;
            Version = version;
            IList<ICommandObject> cmds = new List<ICommandObject>();
            foreach (var c in commands)
            {
                if (cmds.Any(e => e.PerfectCommand == c.PerfectCommand))
                {
                    throw new ArgumentException("There should not be two commands with the same perfect command!");
                }

                cmds.Add(c);
            }

            _commands = cmds;
        }


        public async Task ExecuteCommand(string[] input)
        {
            if (input.Length == 0)
            {
                Console.WriteLine("Please use -v or --help to see information or help!");
                return;
            }

            if (input.Length == 1)
            {
                if (input[0] == "--help")
                {
                    Console.WriteLine(Name);
                    Console.WriteLine("List of Commands:");
                    Console.WriteLine("verbose: -v : show information about this CLI");
                    Console.WriteLine("help : --help : See list of commands");
                    var help = _commands.Select(s => $"{s.Name} : {s.Usage} : {s.Description}").JoinBy("\n");
                    Console.WriteLine(help);
                    return;
                }
                else if (input[0] == "-v")
                {
                    Console.WriteLine(Name);
                    Console.WriteLine("Version: " + Version);
                    Console.WriteLine(Description);
                    Console.WriteLine();
                    Console.WriteLine("See --help for list of commands.");
                    return;
                }
            }

            var rawCommands = input.Where(s => s.Take(2) != "--").ToArray();
            var candidates = _commands.Where(s => CommandMatch(s.Command, rawCommands));
            var commandObjects = candidates as ICommandObject[] ?? candidates.ToArray();
            if (!commandObjects.Any())
            {
                PrintErrorCommand(input);
            }
            else
            {
                var c = commandObjects.OrderByDescending(s => s.Command.Length).First();
                var commands = c.Command;
                IList<string> flags = new List<string>();
                IList<string> rawArguments = new List<string>();
                var variables = new Dictionary<string, string>();
                var counter = 0;
                foreach (var t in input)
                {
                    if (counter != -1 && t == commands[counter])
                    {
                        counter = counter == c.Command.Length - 1 ? -1 : counter + 1;
                    }
                    else
                    {
                        //is flag or variable
                        if (t.Take(2) == "--")
                        {
                            //variable
                            if (t.Contains("="))
                            {
                                var keyValue = t.Skip(2).SplitBy("=").ToArray();
                                //has key
                                if (c.PossibleVariables.Contains(keyValue[0]))
                                {
                                    variables[keyValue[0]] = keyValue.Skip(1).JoinBy("=");
                                }
                                //no key, take it as if its raw argument
                                else
                                {
                                    rawArguments.Add(t);
                                }
                            }
                            //its flag! no = sign
                            else
                            {
                                //if it is really flag
                                var flag = t.Skip(2);
                                if (c.PossibleFlags.Contains(flag))
                                {
                                    flags.Add(flag);
                                }
                                //not flag, add to raw argument    
                                else
                                {
                                    rawArguments.Add(t);
                                }
                            }
                        }
                        else
                        {
                            rawArguments.Add(t);
                        }
                    }
                }

                if (flags.Contains("help"))
                {
                    Console.WriteLine(c.Usage);
                }
                else
                {
                    var result = await c.Execute(input, rawArguments.ToArray(), variables, flags.ToArray());
                    result.Print();
                    
                }
            }
        }


        private void PrintErrorCommand(string[] command)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Unknown Command: " + command.JoinBy(" "));
            Console.ResetColor();
            throw new InvalidProgramException("Unknown Command!");
        }

        private bool CommandMatch(IEnumerable<string> defined, IEnumerable<string> inputted)
        {
            var enumerable = defined as string[] ?? defined.ToArray();
            var enumerable1 = inputted as string[] ?? inputted.ToArray();
            return enumerable.Length <= enumerable1.Length &&
                   enumerable1.Take(enumerable.Count()).SequenceEqual(enumerable);
        }
    }
}