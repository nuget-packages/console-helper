﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Kirinnee.ConsoleHelp
{
    public static class ConsoleHelper
    {
        /// <summary>
        /// Converts Byte to Stream
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static Stream ToStream(this byte[] bytes)
        {
            return new MemoryStream(bytes);
        }

        /// <summary>
        /// Converts Stream to byte. Closes the original stream and diposes it.
        /// </summary>
        /// <param name="input">Stream</param>
        /// <returns></returns>
        public static byte[] ToByte(this Stream input)
        {
            using (var ms = new MemoryStream())
            {
                input.CopyTo(ms);
                input.Close();
                input.Dispose();
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Converts Stream to byte. Original stream still exists and can continue to be used.
        /// </summary>
        /// <param name="input">Stream</param>
        /// <returns></returns>
        public static byte[] ToByteUnsafe(this Stream input)
        {
            using (var ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Read piped in data as byte array
        /// </summary>
        /// <returns></returns>
        public static async Task<byte[]> ReadStdin()
        {
            if (!Console.IsInputRedirected) return new byte[] { };
            using (var stream = new MemoryStream())
            {
                var input = Console.OpenStandardInput();
                await input.CopyToAsync(stream);
                input.Close();
                input.Dispose();
                return stream.ToArray();
            }
        }

        /// <summary>
        /// Write byte array to external data
        /// </summary>
        /// <param name="output"></param>
        /// <returns></returns>
        public static async Task WriteStdout(this byte[] output)
        {
            using (var stream = new MemoryStream(output))
            {
                var stdout = Console.OpenStandardOutput();
                await stream.CopyToAsync(stdout);
                stream.Close();
            }
        }


        /// <summary>
        /// Read piped in data as byte array
        /// </summary>
        /// <returns></returns>
        public static byte[] ReadStdinSync()
        {
            using (var stream = new MemoryStream())
            {
                Stream input = Console.OpenStandardInput();
                input.CopyTo(stream);
                input.Close();
                input.Dispose();
                return stream.ToArray();
            }
        }

        /// <summary>
        /// Write byte array to external data
        /// </summary>
        /// <param name="output"></param>
        /// <returns></returns>
        private static void WriteStdoutSync(this byte[] output)
        {
            using (var stream = new MemoryStream(output))
            {
                var stdout = Console.OpenStandardOutput();
                stream.CopyTo(stdout);
                stream.Close();
            }
        }
    }
}