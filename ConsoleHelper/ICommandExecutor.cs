using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kirinnee.ConsoleHelp
{
    public interface ICommandExecutor
    {
        string Name { get; }
        string Description { get; }
        string Version { get; }
        Task ExecuteCommand(string[] input);
    }
}