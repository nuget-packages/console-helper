# ConsoleHelper

Simple utility methods that helps with console application.

# Getting Started

Install via .NET CLI
```powershell
$ dotnet add package Kirinnee.ConsoleHelper
```

or 

Install via NuGet Package Manager
```powershell
PM> Install-Package Kirinnee.ConsoleHelper
```

# Usage

You will have to import the namespace first!
### Import namespace
```cs
using Kirinnee.ConsoleHelp;
```

## Command Framework

The command FrameWork is an object-oriented framework that allow you to easily create command by
simply implementing the `ICommandObject` interface.

### Implementing ICommandObject
```cs
public class SampleCommand : ICommandObject{
    
    
    public string Name {get;} = "Sample Command";
    public string Descriptiong {get;} = "A command smaple"; 
    public string Usage { get; } = {"sample <message> <options>"};
    public string[] Command {get;} = {"sample"};
    public string PerfectCommand => string.Join("",Command);
    public IEnumerable<string> PossibleFlags {get;} = new [] {"upper", "lower"};
    public IEnumerable<string> PossibleVariables {get;} = new[] {"max"};

    public Task<CommandResult> Execute(string[] fullCommand, string[] rawArgs, Dictionary<string, string> variables, string[] flags){
        //combine all raw arugments into a string. Refer below to find out 
        //what is raw arguments
        var combine = string.Join(" ", rawArgs);

        //if flags has upper, make the combined text upper cased
        if(flags.Contains("upper")) combine = combine.ToUpper();
        //if flags has lower, make the combined text lower cased 
        if(flags.Contains("lower")) combine = combine.ToLower();
        if(variables.ContainsKey("max")){
            var max = int.Parse(variables["max"]);
            combine = string.Join("",combine.ToCharArray().Take(max));
        }

        Console.WriteLine(combine);
        return Task.FromResult(new CommandResult(true, "Printed!"));
    }
}
```

Each of the field has important value in the command

#### Name
This is the name of the command, displayed when --help is used to list all commands

#### Description
this is the description of the command, displayed when --help is used to lsit all commands

#### Usage
This is the usage of the command. It tells the user the command is used. This is displayed when
--help is used to list all commands and when the command fails.

#### Command
This is the actual (sub) command. If it takes more than 1 word, append it to the enumerable 
**in order**. 

#### PerfectCommand
This is signature of the command, please use the auto property getter:
`public string PerfectCommand => string.Join("",Command);`
to ensure two commands with same signature will be flaged.

#### Possible Flags
This defines what can be used as flags. Flags are boolean options that
can be applied to the command. For this example, the user would have to use --upper to trigger the upper flag, and --lower to trigger the lower flag (anywhere in the command).

#### Possible Variables
This defines what variables are used. Variables are used like `--max=5`, which will be converted
into a dictionary with `max` as key and `5` as value.

#### Execute Functions

The execute functions have parameters that help you access the properties of the command.

We will use the Command Object above as an example with the following command: 

`sample Hey! --upper --max=3 What's --up --min=2` 

##### fullCommand
This the full command the user entered, seperated by " " into string array.

Array will be:

 `"sample", "Hey!", "--upper", "--max=3", "What's", "--up", "--min=2"`

##### rawArgs
This is the raw arguments of the command entered. This exclude the origin command (register by the `Command` property), excludes Flags (only those registered by the `Flag` property), and excludes variables (only those register by the `Variable` property).

Array will be:
`"Hey!", "What's", "--up", "--min=2"`

##### variables
This will be variables in a dictionary. The `--` and `=` will be stripped

Dictionary will be :
`min => 2`

##### flags
This will be the list of flags used. They will not include the `--`. 

`upper`

### CommandExecutor

To use these command objects, use the CommandExecutor object.

#### Creating CommandExecutor object
```cs
internal static class Program
{
    internal static void Main(string[] args)
    {
        var exe = new CommandExecutor("Test", "Test console", "0.0.1",
            new List<ICommandObject>() {new Command1(), new Command2()});
        Console.WriteLine(exe._commands.Count());
        Async(args, exe).GetAwaiter().GetResult();
    }

    static async Task Async(string[] args, ICommandExecutor exe)
    {
        await exe.ExecuteCommand(args);
    }
}
```

`new CommandExecuto(name, description, version, IEnumerable<ICommandObject>`

This allows for registration of Name of the CLI, description of the CLI, version of the CLI and
the list of command object to be registered

## Console Utilities

### Convert Stream To Byte Array
Closes and diposes the original stream
```cs
byte[] bytes = stream.ToByte();
```

Does not close or dispose original stream
```cs
byte[] bytes = stream.ToByteUnsafe();
```

### Convert Byte to Stream
```cs
Stream streams = btyes.ToStream();
```

### Cat-Type Program
Read redirected Stdin as bytes
```cs
//Asynchronous
byte[] stdin = await ReadStdin();
//Synchronous
byte[] stdin = ReadStdinSync();
```

Pipe bytes to Stdout stream
```cs
//Asynchronous
await WriteStdout(bytes[]);
//Synchronous
WriteStdoutSync();
```




## Contributing
Please read [CONTRIBUTING.md](CONTRIBUTING.MD) for details on our code of conduct, and the process for submitting pull requests to us.


## Authors
* [kirinnee](mailto:kirinnee@gmail.com) 

## License
This project is licensed under MIT - see the [LICENSE.md](LICENSE.MD) file for details